<?php

namespace App\Http\Controllers;

use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

use App\Admin;

class AdminController extends Controller
{
    public function index(){
        $id = Auth::id();
        $info = Admin::select('name', 'login', 'image')->where('id', $id)->first();

        return view('admin.index')->with('info', $info);
    }

    public function ajax(){
        $id = Auth::id();
        $info = Admin::select('name', 'login', 'image')->where('id', $id)->first();

        return \Response::json($info);
    }

    public function changes(Request $request){
        $this->validate($request,[
            'name'         => 'required',
            'login'        => 'required',
        ]);
        $admin =  Admin::where('login', $request['login'])->first();
        if (($request['login'] != Auth::user()->login) && (!is_null($admin))){
            return \Response::json(['login_error' => true]);
        }



        $login = Auth::user()->login;
        $admin =  Admin::where('login', $login)->first();


        if($admin){

            $admin->name = $request['name'];
            $admin->login = $request['login'];
            if(!is_null($request['old_password']) ){
                $this->validate($request,[
                    'new_password'         => 'required',
                ]);

                if(Hash::check($request['old_password'], Auth::user()->password)){
                    $new_password = Hash::make($request['new_password']);
                    $admin->password = $new_password;
                }else{
                    return \Response::json(['password_error' => true]);

                }
            }
            elseif(!is_null($request['new_password'])){
                $this->validate($request,[
                    'old_password'        => 'required',
                ]);
            }




            if ((isset($request['profile_image']))  and (!empty($request['profile_image']))){


                if($request['profile_image'] == 'none' and Auth::user()->image != 'none.png'){
                    $admin->image = 'none.png';

                    $admin_image = public_path() .'/admin_assets/assets/images/users/'. Auth::user()->image;
                    File::delete($admin_image);

                }
                else{
                    $validator = Validator::make($request->all(),[
                        'profile_image'         => 'base64image|base64max:300000',
                    ]);
                    if ($validator->fails()) {
                        dd($validator->errors());
                    }


                    $image = $request['profile_image'];
                    $image = str_replace('data:image/jpeg;base64,', '', $image);
                    $image = base64_decode(str_replace(' ', '+', $image));

                    $admin_image = public_path() .'/admin_assets/assets/images/users/'. Auth::user()->image;
                    if((Auth::user()->image != 'none.png') && (File::exists($admin_image))){
                        File::delete($admin_image);
                        $v = explode( '-',$admin_image);
                        $v = $v[1];
                        preg_match_all('!\d+!', $v, $version);
                        $version = $version[0][0]+1;
                        $imageName = Auth::id() .'-'.$version.'.'.'jpeg';
                    }
                    else{
                        $imageName= Auth::id() .'-1.'.'jpeg';
                    }

                    $admin->image = $imageName;
                    File::put(public_path() .'/admin_assets/assets/images/users/' . $imageName, $image);

                }

            }
            $admin->save();
        }
        return redirect()->route('admin.index');
    }
}
