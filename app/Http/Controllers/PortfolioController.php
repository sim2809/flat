<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use phpDocumentor\Reflection\DocBlock\Description;
use Illuminate\Support\Facades\Validator;
use App\Portfolio;
class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portfolios = Portfolio::all();
        return view('admin.portfolio')->with('portfolios', $portfolios);
    }

    public function ajax(){
        $portfolios = DB::table('portfolios')
            ->join('categories', 'portfolios.category_id', '=', 'categories.id')
            ->select('portfolios.*', 'categories.name')
            ->get();
        $categories = Category::all();
        return response()->json(['portfolios' => $portfolios, 'categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'title'       => 'required',
           'description' => 'required|min:10',
           'file'        => 'required|image|mimes:jpeg,png,jpg',
           'category_id' => 'required|numeric',
        ]);

        $portfolio = DB::table('portfolios')
            ->where('title', '=', $request['title'])
            ->first();


        if(!is_null($portfolio)){
            return response()->json(['title_error' => 'such title is already used']);
        }


        $image = $request->file('file');
        $image_extension = $request->file('file')->getClientOriginalExtension();
        $image_name = $request->title .'.'. $image_extension;

        $image->move(public_path('images/portfolio_images'), $image_name);

        $form_data = array(
            'title'       => $request->title,
            'description' => $request->description,
            'image'       => $image_name,
            'category_id' => $request->category_id,
        );

        $category = Category::where('id', '=', $form_data['category_id'])->first();
        if ($category === null) {
            $form_data['category_id'] = 1;
        }

        Portfolio::create($form_data);

        return response()->json();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $info = Portfolio::find($id);
        return  \Response::json($info);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

//        $title_valid = Validator::make($request->all(),[
//            'title'  => 'required'
//        ]);
//        $descrption_valid = Validator::make($request->all(),[
//            'description'  => 'required'
//        ]);
//
//        if ($title_valid->fails()) {
//            dd('aaa');
//            return response()->json('title_error',true);
//        }
//        else if($descrption_valid->fails()){
//            return response()->json('description_error',true);
//        }

        $validation = Validator::make($request->all(), [
            'portfolio_id'       => 'required|numeric',
            'description'  => 'required',
            'title'  => 'required',
            'file'        => 'image|mimes:jpeg,png,jpg',
            'category_id' => 'required|numeric',
        ]);

        if ($validation->fails())
        {
            return response()->json(['errors'=>$validation->errors()->all()]);
        }

        $portfolio_check = DB::table('portfolios')
            ->where('title', '=', $request['title'])
            ->first();

        $portfolio = Portfolio::find($request['portfolio_id']);
        if(!is_null($portfolio_check) && ($request['title'] != $portfolio->title)){
            return response()->json(['title_error' => 'such title is already used']);
        }

        if ((isset($request['file'])) && (!empty($request['file']))){
            $portfolio_image = public_path() .'/images/portfolio_images/' . $portfolio->image;

            if (File::exists($portfolio_image)){
                File::delete($portfolio_image);

                $image = $request->file('file');
                $image_extension = $request->file('file')->getClientOriginalExtension();
                $image_name = $request->title .'.'. $image_extension;

                $image->move(public_path('images/portfolio_images'), $image_name);
            }
            $portfolio->image      = $image_name;
        }

        $portfolio->title            = $request['title'];
        $portfolio->description      = $request['description'];
        $portfolio->category_id      = $request['category_id'];
        $portfolio->save();

        return response()->json(['ok'=>'ok']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $portfolio = DB::table('portfolios')
            ->where('id', '=', $id)
            ->first();

        $portfolio_image = public_path() .'/images/portfolio_images/' . $portfolio->image;

        if (File::exists($portfolio_image)) {
            File::delete($portfolio_image);
        }
        Portfolio::destroy($id);
        echo 'ok';
    }
}