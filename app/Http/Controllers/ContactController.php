<?php

namespace App\Http\Controllers;
use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactAnswerMail;

class ContactController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = contact::all();

        return view('admin.contacts', ['contacts' => $contacts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'subject' => 'required|max:255',
            'message' => 'required',
        ]);

        contact::create($request->all());

        return redirect()->route('index');
    }


    public function show(Request $request)
    {
        //dd($request);
        $data = Contact::find($request['id']);

        return \Response::json($data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Contact::destroy($id);
        echo 'ok';
    }

    public function contact_answer(Request $request){
        $this->validate($request,[
            'id'      => 'required',
            'email'   => 'required',
            'subject' => 'required',
            'message' => 'required',
        ]);

        $data = array(
            'name'     => $request['name'],
            'email'    => $request['email'],
            'subject'  => $request['subject'],
            'message'  => $request['message'],

        );
        Mail::to($data['email'])->send(new ContactAnswerMail($data));

        Contact::destroy($request['id']);
    }

    public function ajax(Request $request)
    {
        $data = Contact::all();

        return \Response::json($data);

    }
}