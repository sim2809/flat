<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Testimonial;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class TestimonialsController extends Controller
{


    public function index(Request $request)
    {
        $testimonials = DB::table('testimonials')
            ->join('admins', 'testimonials.admin_id', '=', 'admins.id')
            ->select('testimonials.*', 'admins.name', 'admins.image')
            ->get();

        return view('admin.testimonials');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'testimonial' => 'required|min:10'
        ]);
        $data = array(
            'text' => $request['testimonial'],
            'admin_id' => Auth::id(),
        );
        Testimonial::create($data);

        return redirect()->route('testimonials.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|numeric'
        ]);

        $data = Testimonial::find($request['id']);

        return \Response::json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $testimonial = Testimonial::find($id);

        if((!is_null($testimonial))&&(Auth::id() == $testimonial->admin_id)){
            Testimonial::destroy($id);
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function ajax()
    {
        $testimonials = DB::table('testimonials')
            ->join('admins', 'testimonials.admin_id', '=', 'admins.id')
            ->select('testimonials.*', 'admins.name', 'admins.image')
            ->get();

        foreach($testimonials as $testimonial){
            if($testimonial->admin_id ==    Auth::id()){
                $testimonial->is_auther = true;
            }
        }

        return Response::json($testimonials);
    }
}
