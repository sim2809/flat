<?php

namespace App\Http\Controllers;

use App\Category;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Portfolio;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index(){
        $portfolios = Portfolio::all();
        $categories = Category::all();
        $testimonials = DB::table('testimonials')
            ->join('admins', 'testimonials.admin_id', '=', 'admins.id')
            ->select('testimonials.*', 'admins.name', 'admins.image')
            ->get();

        $all_categories_ids = '';
        foreach ($categories as $category){
            $all_categories_ids = $all_categories_ids . ' ' . $category->id;
        }
        $data = array(
            'portfolios'            => $portfolios,
            'categories'            => $categories,
            'all_categories_ids'    => $all_categories_ids,
            'testimonials'          => $testimonials,
        );

        return view('index')->with($data);
    }

    public function modal(Request $request){
        $this->validate($request, [
            'id' =>'required'
        ]);
        $data = Portfolio::find($request['id']);

        return \Response::json($data);
    }
}
