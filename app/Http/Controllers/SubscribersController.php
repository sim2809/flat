<?php

namespace App\Http\Controllers;

use http\Env\Response;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use App\Subscriber;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\Mail\FirstSubscriberMail;
use App\Mail\SendSubscribersMail;
class SubscribersController extends Controller
{
    use ValidatesRequests;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscribers = Subscriber::select('email')->where('verification', '=', 'true')->get();
        return view('admin.subscribers', ['subscribers' => $subscribers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $checker = Subscriber::where('email', '=', $request['sub_email'])->count();
        if($checker > 0){
            return response()->json(['error' => true]);
        }

        do{
            $token = Hash::make(Str::random(32));
        }
        while(Subscriber::where('verification', '=', $token)->count() > 0);

        $data = array(
            'email' => $request['sub_email'],
            'verification' => $token,
        );
        Subscriber::create($data);

        Mail::to($request['sub_email'])->send(new FirstSubscriberMail($token));

        return response()->json();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function send_email(Request $request){
        $this->validate($request,[
            'subject' => 'required|max:255',
            'message' => 'required',
        ]);

        $data = array(
            'subject' => $request['subject'],
            'message' => $request['message'],
        );

        $emails = Subscriber::select('email')->where('verification', '=', 'true')->get();

        foreach ($emails as $email){
            Mail::to($email)->send(new SendSubscribersMail($data));
        }

        return redirect()->route('subscribers')->with('success', 'mails sent successfully');

    }


    public function verification(Request $request){
        $this->validate($request,[
            'token' => 'required',
        ]);
        $token = $request['token'];

        $subscriber = Subscriber::where('verification', '=', $token)->first();
        if($subscriber != null and $token != 'true'){
            $subscriber->verification = 'true';
            $subscriber->save();
            return redirect()->route('index')->with('success', 'subscribing completed successfully');
        }
        return redirect()->route('index')->with('error', 'subscribing completed unsuccessfully');


    }
}
