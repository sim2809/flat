<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    protected function index(){
        return view('login');
    }

    protected function login_check(Request $request)
    {
        $request->validate([
            'login' => 'required',
            'password' => 'required|alphaNum|min:3'
        ]);

        $user_data = array(
            'login' => $request->get('login'),
            'password' => $request->get('password'),
        );

        if (Auth::attempt($user_data)) {
            return redirect()->route('admin.index');
        } else {
            return back()->withErrors(['login details was wrong']);
        }
    }


    protected function login_success(){
            return view('login_success');
        }


    protected function logout(){
            Auth::logout();
            return redirect()->route('index');
        }


}
