<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FirstSubscriberMail extends Mailable
{
    use Queueable, SerializesModels;
    private $token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('galstyan.simon12@gmail.com')->subject('First Email From Flat')->view('admin.mail_views.Subscribers_first_mail')->with('token', $this->token);
    }

}
