<?php

use Illuminate\Database\Seeder;
use App\Admin;
class AdminTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $password = "admin";
        Admin::create([
            'name'                =>  'Simon Galstyan',
            'login'               =>  'admin',
            'password'            =>  Hash::make($password),
            'image'               =>  '1-1.png',
            'remember_token'      =>  Str::random(10)
        ]);


        $password = "123";
        Admin::create([
            'name'                =>  'Other Admin',
            'login'               =>  '123',
            'password'            =>  Hash::make($password),
            'image'               =>  'none.png',
            'remember_token'      =>  Str::random(10)
        ]);

    }
}

