<?php

use Illuminate\Database\Seeder;
use App\Testimonial;
class TestimonialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Testimonial::create([
            'text'                =>  'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eum explicabo maiores quaerat quod sit vero voluptatem? Beatae corporis debitis dolore, dolores, explicabo mollitia natus nobis quae sunt temporibus voluptas.',
            'admin_id'               =>  1,
        ]);

        Testimonial::create([
            'text'                =>  'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque eum explicabo maiores quaerat quod sit vero voluptatem? Beatae corporis debitis dolore, dolores, explicabo mollitia natus nobis quae sunt temporibus voluptas.',
            'admin_id'               =>  2,
        ]);

    }
}
