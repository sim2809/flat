<?php

use Illuminate\Database\Seeder;
use App\Portfolio;
class PortfoliosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Portfolio::create([
            'title'               =>  'Iphone 6 concnet',
            'description'         =>  'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet aspernatur commodi dolor, doloremque dolores eos est exercitationem hic, inventore iste minus nam nihil quidem quis rerum, suscipit ullam vel.',
            'image'               =>  'Iphone 6 concnet.jpg',
            'category_id'               =>  2,
        ]);

        Portfolio::create([
            'title'               =>  'Web',
            'description'         =>  'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet aspernatur commodi dolor, doloremque dolores eos est exercitationem hic, inventore iste minus nam nihil quidem quis rerum, suscipit ullam vel.',
            'image'               =>  'Web.jpg',
            'category_id'               =>  1,
        ]);

    }
}
