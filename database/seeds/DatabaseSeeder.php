<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();
        $this->call(AdminTablesSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(PortfoliosTableSeeder::class);
        $this->call(TestimonialsTableSeeder::class);
    }
}
