<?php

use Illuminate\Database\Seeder;
use App\Category;
class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Category::create([
            'name'                =>  'Uncategory',
        ]);

        Category::create([
            'name'                =>  'Web',
        ]);

        Category::create([
            'name'                =>  'IOS',
        ]);

    }
}
