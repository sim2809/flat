<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', 'UserController@index')->name('index');

Route::post('user/portfolio', 'UserController@modal')->name('portfolio.index');

Route::post('contact/send', 'ContactController@store')->name('contact.send');



    Route::get('/login', 'LoginController@index')->name('login')->middleware('guest');

    Route::post('/login/check', 'LoginController@login_check')->name('login.check');

    Route::get('logout', 'LoginController@logout')->name('logout');



Route::group(['middleware' => ['auth']], function () {

    Route::get('admin', 'AdminController@index')->name('admin.index');

    Route::get('admin/ajax', 'AdminController@ajax')->name('admin.ajax');

    Route::post('admin/changes', 'AdminController@changes')->name('admin.changes');

    Route::resource('contacts', 'ContactController');

    Route::post('contacts/ajax', 'ContactController@ajax')->name('contact.show');

    Route::post('contact/show', 'ContactController@show')->name('contact.show');

    Route::post('contact/answer', 'ContactController@contact_answer')->name('contact.answer');

    Route::get('subscribers/show', 'SubscribersController@index')->name('subscribers');

    Route::post('subscribers/email', 'SubscribersController@send_email')->name('subscribers.email');

    Route::resource('category','CategoryController');

    Route::resource('portfolio','PortfolioController');

    Route::post('portfolio/update', 'portfolioController@update')->name('portfolio.update');

    Route::resource('testimonials','TestimonialsController');

    Route::post('testimonials/ajax','TestimonialsController@ajax')->name('testimonials.ajax');

    Route::post('testimonials/show','TestimonialsController@show')->name('testimonials.show');

    Route::post('portfolio/ajax','PortfolioController@ajax')->name('portfolio.ajax');
});




Route::post('subscribe/store', 'SubscribersController@store')->name('subscribe.store');
Route::get('subscribe/verification', 'SubscribersController@verification')->name('subscribe.verification');
