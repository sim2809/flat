$(document).ready(function(){

    function name_validator(){
        var name = $("input[name=name]");
        var result = $("#name_validate");
        var regex = /^[a-z A-Z]+$/;
        result.text("");
        if(name.val() === ''){
            result.text("name can not be empty");
            result.css("color", "red");
            name.focus();
            return false
        }
        else if (!regex.test(name.val())) {
            result.text("name isn't valid");
            result.css("color", "red");
            name.focus();
            return false
        }
        return true
    }

    function login_validator(){
        var login = $("input[name=login]");
        var result = $("#login_validate");
        result.text("");
        if(login.val() === ''){
            result.text("subject can not be empty");
            result.css("color", "red");
            login.focus();
            return false
        }
        else if(login.val().length > 17){
            result.text("the subject's length should not be more than 17 letters");
            result.css("color", "red");
            login.focus();
            return false
        }
        return true
    }

    function passwords_validator_1(){
        console.log('a');
        var new_passwrod = $("input[name=new_password]");
        var old_passwrod = $("input[name=old_password]");
        var result = $("#first_p_validate");
        result.text("");
        if(old_passwrod.val().length < 5){
            result.text("the password's length should be more than 5 letters");
            result.css("color", "red");
            old_passwrod.focus();
            return false
        }
        return true
    }

    function passwords_validator_2(){
        var old_passwrod = $("input[name=old_password]");
        var new_passwrod = $("input[name=new_password]");
        var result = $("#second_p_validate");
        var result_2 = $("#first_p_validate");
        result.text("");
        if(new_passwrod.val() !== '' && old_passwrod.val() === ''){
            result_2.text("old password can not be empty");
            result_2.css("color", "red");
            return false
        }
        else if(new_passwrod.val() === '' && old_passwrod.val() !== ''){
            result.text("new password can not be empty");
            result.css("color", "red");
            new_passwrod.focus();
            return false
        }
        else if(new_passwrod.val().length < 5 && old_passwrod.val() !== ''){
            result.text("the password's length should be more than 5 letters");
            result.css("color", "red");
            new_passwrod.focus();
            return false
        }

        return true
    }

    function changes_validate(){
        if (
            name_validator()&&
            login_validator()&&
            passwords_validator_2()
        )
        {
            return true
        }
        return false
    }

    $("input[name=name]").on("keyup",  name_validator);
    $("input[name=login]").on("keyup",  login_validator);
    $("input[name=old_password]").on("keyup",  passwords_validator_1);
    $("input[name=new_password]").on("keyup",  passwords_validator_2);


    $("#admin_changes").submit(function(event){

        event.preventDefault();

        if(changes_validate() && $('#admin_changes').valid()){

            $.ajax({
                url:  $(this).attr('action'),
                type: 'post',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function() {
                    $("#changes_loading").css('display', 'block');
                },

                success: function(data) {
                    $("#changes_loading").css('display', 'none');
                    if (data.login_error) {
                        $("#login_validate").text('such login is already used');
                    }
                    if (data.password_error) {
                        $("#first_p_validate").text('your old password was entered incorrectly');
                    }
                    else {
                            Swal.fire(
                                'settings has changed successfully',
                                '',
                                'success'
                            );
                        }
                    show_info();
                }

                });
        }

    });

    function show_info(){

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: 'admin/ajax',
            type: "get",
            dataType: 'json',
            data:{},

            beforeSend: function () {
                $("#changes_loading").css('display', 'block');
            },
            success:function(data)
            {
                $("#changes_loading").css('display', 'none');
                $("input[name=name]").val(data.name);
                $("input[name=login]").val(data.login);
                //$(".profile_image").attr('href', data.image);

            }
        });

    }

    $("#admin_changes").validate({
        errorPlacement: function(error, element) {
            if (element.hasClass("image-validate")) {
                error.insertAfter('#image-lable');
            }
            else{
                error.insertAfter(element);
            }
        },
        ignore: "",
        rules:{
            image:{
                required: false,
                extension: "png|jpe?g",
                filesize : 3000000,
            },
        },
        messages:{
            image:{
                extension: "<p style='color:red'>file must be png, jpg or jpeg<p>",
                filesize: "<p style='color:red'>file is very big<p>",
            }

        }

    });


    function del_btn_show(){
        var check = $('#profile_image').attr('src');
        check = image_name(check);
        if (check === 'none.png'){
            $('#delete_profile_image_btn').hide();
        }
        else{
            $('#delete_profile_image_btn').show();
        }
    }

    function image_name(src){
        src = src.split('/');
        src = src[src.length-1];
        return src;
    }



    del_btn_show();



    $(document).on('click', "#delete_profile_image_btn", function(){
        var old_img = $('#profile_image_old_hidden').val();
        //console.log(old_img);
        var old_img_check = image_name(old_img);
        console.log(old_img_check);

        console.log('none');
        $('#profile_image').attr('src', 'admin_assets/assets/images/users/none.png');
        $('#hidden_image_input').val('none');
        $('#delete_profile_image_btn').hide();


    });


    $(document).on('click', "#hide_image_cropper", function(){
        $('#hide_image_cropper').hide();
        $('.cr-boundary').hide();
        $('.cr-slider-wrap').hide();

        $('#profile_image').attr('src', $('#profile_image_old_hidden').val());
        $('#profile_image').show();

        del_btn_show();
        $('#add_profile_image_btn').hide();
        $('#hidden_image_input').val('');
    });



    $('#hide_image_cropper').hide();
    $('#add_profile_image_btn').hide();

    function readURL(input) {
        $('.cr-boundary').hide();
        $('.cr-slider-wrap').hide();
        $('#hidden_image_input').val('');

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {

                $('#add_profile_image_btn').show();
                $('#hide_image_cropper').show();
                $('#delete_profile_image_btn').hide();
                $('#profile_image').attr('src', e.target.result);
                $('#profile_image').hide();

                var el = $('#profile_image')[0];
                var vanilla = new Croppie(el, {
                    viewport: { width: 120, height: 120, type: 'circle' },
                    boundary: { width: 200, height: 200 },
                    showZoomer: true,
                    enableOrientation: true,
                    mouseWheelZoom: 'ctrl'
                });
                vanilla.bind({
                    url: e.target.result,
                    orientation: 1
                });
                $(document).on('click', "#add_profile_image", function(){

                    $('#add_profile_image_btn').hide();
                    $('#hide_image_cropper').hide();
                    del_btn_show();


                    vanilla.result({
                        type: 'base64',
                        format: 'jpeg',
                        size: 'original'
                    }).then(function (resp) {
                        $('#profile_image_input').val('');
                        $('.cr-boundary').hide();
                        $('.cr-slider-wrap').hide();
                        $('#hidden_image_input').val(resp);
                        $('#profile_image').attr('src', resp);
                        $('#profile_image').show();

                    });
                });

            };

            reader.readAsDataURL(input.files[0]);
        }
    }




    $("#profile_image_input").change(function() {
        if($('input[name=image]').valid())
        {
            console.log('asasas');
            readURL(this);
        }
    });

});