$(document).ready(function() {
            portfolio_show();

            $(document).on('click', ".edit_portfolio", function(){
            var id = $(this).attr('id');
            id = id.replace( /^\D+/g, '');
            console.log(id);
            portfolio_edit(id);

        });
        $(document).on('click', ".portfolio_show_more", function(){
            var id = $(this).attr('id');
            id = id.replace( /^\D+/g, '');
            console.log(id);
            portfolio_edit(id);

        });

        function portfolio_edit(id){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: 'portfolio/'+id,
                type: "get",
                dataType: 'json',
                data: {},

                success:function(info)
                {
                    console.log(info.category_id);
                    $('#edit_portfolio_modal').modal('show');
                    $('#edit_portfolio_title').val(info.title);
                    $('#edit_portfolio_description').val(info.description);
                    $("#category_p_select_edit").val(info.category_id);
                    $('#edit_portfolio_id').val(info.id);
                    $('#portfolio-img-tag').attr('src', 'images/portfolio_images/'+info.image);

                    console.log('success');
                }
            });
        }

        $(document).on('submit', "#edit_portfolio_form", function(event){
            event.preventDefault();
            $.ajax({
                url: $(this).attr('action'),
                type: "post",
                dataType: 'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success:function(data)
                {
                    console.log(data.errors);
                    if(data.errors === undefined){
                        var any_error = 0;
                    }
                    if(data.title_error !== undefined){
                        console.log(data.title_error);
                        $('#title_error').show();
                        $('#title_error').html(data.title_error);
                        var any_error = 1;
                    }

                    jQuery.each(data.errors, function(key, value){
                        console.log(value);
                        if (value === 'The title field is required.'){
                            $("#edit_portfolio_title").focus();
                            $('#title_error').html('please enter title');
                            var any_error = 1;
                        }
                        else if(value === 'The description field is required.'){
                            $("#edit_portfolio_description").focus();
                        }
                        else if (value !== 'The title field is required.'){
                            $('#title_error').hide();
                        }

                        if(value === 'The description field is required.'){
                            $('#description_error').html('please enter description');
                            var any_error = 1;
                        }
                        else{
                            $('#description_error').hide();
                        }

                    });

                        if (any_error === 0){
                            $('#title_error').hide();
                            $('#description_error').hide();
                            $('#edit_portfolio_modal').modal('hide');
                            portfolio_show();
                        }


                }
            });

        });

        function portfolio_show(){
            $('#category_p_list').html('');
            $('#portfolio_list').html('');
            $('#category_p_select').html('');
            $('#category_p_select_edit').html('');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: 'portfolio/ajax',
                type: "post",
                dataType: 'json',
                success:function(data)
                {
                    console.log(data.portfolios);
                    $.each(data.portfolios, function(i, item) {
                        $('#portfolio_list').append(
                            '                   <tr>' +
                            '                                        <td><img src="images/portfolio_images/'+ item.image + '" alt="portfolio image" width="100px"></td>' +
                            '                                        <td>'+ item.title +'</td>' +
                            '                                        <td>'+ item.description.slice(0,10) +' <button type="button" class="portfolio_show_more" id="'+ item.id +'">. . .</button></td>' +
                            '                                        <td>'+ item.name +'</td>' +
                            '                                        <td   style="float: right; border: 0;">' +
                            '                                            <button type="button" class="edit edit_portfolio" id="edit-'+item.id+'" data-toggle="modal"  ><i class="fa fa-pencil-square-o p_edit" aria-hidden="true" style="font-size:24px"></i></button>' +
                            '                                            <button type="button" href="portfolio/destroy" class="edit trash_portfolio" id="'+ item.id +'"><i class="fa fa-trash p_delete" aria-hidden="true" style="font-size:24px"></i></button>' +
                            '                                        </td>'
                        );
                    });

                    $.each(data.categories, function(i, item) {
                        var category_id = item.id;
                        item.id = 'category-' +item.id;

                        if(item.id == 'category-1'){
                            $('#category_p_list').append('<div><p class="category_list" style="display: inline">-'+item.name+'</p></div>');
                        }
                        else{
                            $('#category_p_list').append('<div><p class="category_list" style="display: inline">-'+item.name+'</p><i id='+item.id+' class="fa fa-trash category_delete" aria-hidden="true" style="font-size:15px;display: inline"></i></div>');
                        }

                        $('#category_p_select').append('<option value='+category_id+'>'+item.name+'</option>');

                        $('#category_p_select_edit').append('<option value='+category_id+'>'+item.name+'</option>');


                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
        }


    $("#add_portfolio_form").validate({
        rules:{
            title:{
                required: true
            },

            description:{
                required: true,
                minlength: 10,
            },

            file:{
                required: true,
                extension: "png|jpe?g",
                filesize: 30000000
            }
        },
        messages:{
            title: "<p style='color:red'>please enter Title<p>",
            description: {
                required: "<p style='color:red'>please enter Description<p>",
                minlength: "<p style='color:red'>please enter minimum 10 words<p>"
            },
            file: {
                filesize: "<p stylsizee='color:red'>file size must be less then 300mb and more than 1kb<p>",
                required: "<p style='color:red'>please upload project's image<p>",
                extension: "<p style='color:red'>file must be png, jpg or jpeg<p>",
            }
        }

    });

    $("#add_portfolio_form").submit(function(event){
        event.preventDefault();
        console.log('a');



        if($("#add_portfolio_form").valid() === true){

            $.ajax({
                url:  $(this).attr('action'),
                type: 'post',
                dataType: 'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    console.log("loading");
                },
                success: function (data) {

                        if(data.title_error !== undefined){
                            console.log(data.title_error);
                            $('#portfolio_error').show();
                            $('#portfolio_error').html('such title is already used');
                        }
                        else {
                            $('#portfolio_error').hide();
                            $('#add_portfolio_modal').modal('hide');
                            portfolio_show();
                            Swal.fire(
                                'Portfolio crated successfully',
                                '',
                                'success'
                            );
                        }

                },

            });
        }
        else{
            $('#portfolio_error').hide();
        }
    });


    $(document).on('click', ".trash_portfolio", function(){
        Swal.fire({
            title: 'Are you sure?',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                var id = $(this).attr('id');
                id = id.replace( /^\D+/g, '');
                console.log(id);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: 'portfolio/'+id,
                    type: "delete",
                    dataType: 'html',
                    data:{
                        "id": id,
                        "_method": 'DELETE',
                    },
                    success:function(data)
                    {
                        console.log(data);
                        Swal.fire(
                            'Deleted!',
                            'portfolio has been deleted.',
                            'success'
                        );
                        portfolio_show();

                    }
                });

            }
        });


    });


    $(document).on('click', ".category_delete", function(){
            var id = $(this).attr('id');
            id = id.replace( /^\D+/g, '');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: 'category/'+id,
                type: "delete",
                dataType: 'html',
                data: {"id": id,},

                success:function()
                {
                    portfolio_show();
                }
            });

        });

            $("#add_category_form").validate({
                rules:{
                    category_name:{
                        required: true,
                        maxlength: 20
                    },
                },
                messages:{
                    category_name: {
                        required: "<p style='color:red'>please enter Category name<p>",
                        maxlength: "<p style='color:red'>please enter maximum 20 words<p>"

                    }
                },
                errorLabelContainer : '.category_error',
            });

            $("#add_category_form").submit(function(event){
                if($("#add_category_form").valid() === true){
                    event.preventDefault();
                    console.log('a');
                    $.ajax({
                        url:  $(this).attr('action'),
                        type: 'post',
                        data: new FormData(this),
                        contentType: false,
                        cache: false,
                        processData: false,

                        beforeSend: function () {
                        },
                        success: function () {
                            portfolio_show();



                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.status);
                            alert(ajaxOptions);
                            alert(thrownError);
                        }
                    });
                }
            });



    $("#edit_portfolio_form").validate({
        rules:{
            file:{
                extension: "png|jpe?g",
                filesize: 300000
            }
        },
        messages:{
            
            file: {
                filesize: "<p style='color:red'>file size must be less then 300mb and more than 1kb<p>",
                extension: "<p style='color:red'>file must be png, jpg or jpeg<p>",

            }
        }

    });


            $("#portfolio-edit-img").change(function() {
                if($('#portfolio-edit-img').valid()) {
                    portfolio_edit_image(this);
                }
                else{
                    $('#portfolio-img-tag').hide();
                }
            });



            function portfolio_edit_image(input) {


                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#portfolio-img-tag').show();
                        $('#portfolio-img-tag').attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }


    $('#portfolio-add-img-tag').hide();
    $("#portfolio-add-img").change(function() {
        if($('#portfolio-add-img').valid()) {
            portfolio_add_image(this);
        }
        else{
            $('#portfolio-add-img-tag').hide();
        }
    });



    function portfolio_add_image(input) {


        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#portfolio-add-img-tag').show();
                $('#portfolio-add-img-tag').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }


});