$(document).ready(function(){

        testimonial_show();

        $("#add_testimonial_form").validate({
            rules:{
                testimonial:{
                    required: true,
                    minlength: 10,
                }
            },
            messages:{
                testimonial:{
                    required: "<p style=\'color:red\'>please enter testimonial</p>",
                    minlength: "<p style='color:red'>please enter minimum 10 words<p>"
                }
            }

        });


        $("#add_testimonial_form").submit(function(event){
            event.preventDefault();
            console.log('a');



            if($("#add_testimonial_form").valid() === true){

                $.ajax({
                    url:  $(this).attr('action'),
                    type: 'post',
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,

                    beforeSend: function () {
                        console.log("loading");
                    },
                    success: function () {
                        $('#add_testimonials_modal').modal('hide');
                        Swal.fire(
                            'Testimonial crated successfully',
                            '',
                            'success'
                        );
                        testimonial_show();

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                        alert(ajaxOptions);
                        alert(thrownError);
                    }
                });
            }
        });

        function testimonial_show(){
            $('#testimonial_list').html('');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: 'testimonials/ajax',
                type: "post",
                dataType: 'json',
                success:function(data)
                {
                    $.each(data, function(i, item) {
                        console.log(item.image);
                        if(item.is_auther){
                            $('#testimonial_list').append(
                                '                   <tr>' +
                                '                                        <td><img src="admin_assets/assets/images/users/'+ item.image + '" alt="portfolio image" width="100px" class="testimonial_user_image"></td>' +
                                '                                        <td>'+ item.name +'</td>' +
                                '                                        <td>'+ item.text.slice(0,10) +' <button type="button" class="testimonial_show_more" id="show-testimonial'+ item.id +'">. . .</button></td>' +
                                '                                        <td   style="float: right; border: 0;">' +
                                '                                            <button type="button" href="portfolio/destroy" class="edit trash_testimonial" id="delete-testimonials-'+ item.id +'"><i class="fa fa-trash p_delete" aria-hidden="true" style="font-size:24px"></i></button>' +
                                '                                        </td>'
                            );
                        }
                        else{
                            $('#testimonial_list').append(
                                '                   <tr>' +
                                '                                        <td><img src="admin_assets/assets/images/users/'+ item.image + '" alt="portfolio image" width="100px" class="testimonial_user_image"></td>' +
                                '                                        <td>'+ item.name +'</td>' +
                                '                                        <td>'+ item.text.slice(0,10) +' <button type="button" class="testimonial_show_more" id="show-testimonial'+ item.id +'">. . .</button></td>'
                            );

                        }
                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
}

    $(document).on('click', ".trash_testimonial", function(){
        Swal.fire({
            title: 'Are you sure?',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                var id = $(this).attr('id');
                id = id.replace( /^\D+/g, '');
                console.log(id);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: 'testimonials/'+id,
                    type: "delete",
                    dataType: 'html',
                    data:{
                        "id": id,
                        "_method": 'DELETE',
                    },
                    success:function(data)
                    {
                        console.log(data);
                        if(data !== 'error'){
                            Swal.fire(
                                'Deleted!',
                                'Testimonial has been deleted.',
                                'success'
                            );
                        }
                        else{
                            Swal.fire(
                                'Error',
                                '',
                                'error'
                            );
                        }

                        testimonial_show();

                    }
                });

            }
        });


    });

        $(document).on('click', ".testimonial_show_more", function(){

                $('#testimonial_show').modal('show');

                var id = $(this).attr('id');
                id = id.replace( /^\D+/g, '');
                console.log(id);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: 'testimonials/show',
                    type: "post",
                    dataType: 'json',
                    data:{
                        "id": id,
                    },
                    success:function(data)
                    {
                        $('#testimonial_text').html(data.text);
                    }
                });

        });
});