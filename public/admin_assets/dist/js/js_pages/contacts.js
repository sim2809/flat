$(document).ready(function(){

    $("#add_testimonial_form").submit(function(event){
        event.preventDefault();
        console.log('a');



        if($("#add_testimonial_form").valid() === true){

            $.ajax({
                url:  $(this).attr('action'),
                type: 'post',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    console.log("loading");
                },
                success: function () {
                    $('#add_testimonials_modal').modal('hide');
                    Swal.fire(
                        'Testimonial crated successfully',
                        '',
                        'success'
                    );
                    testimonial_show();

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(ajaxOptions);
                    alert(thrownError);
                }
            });
        }
    });

    function contact_show(){
        $('#contact_show').html('');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: 'contacts/ajax',
            type: "post",
            dataType: 'json',
            success:function(data)
            {
                $.each(data, function(i, item) {

                        $('#contact_show').append('      <tr>' +
                        '                                    <td class="txt-oflo">'+item.name+'</td>' +
                        '                                    <td>       '+item.email+'</td>' +
                        '                                    <td class="txt-oflo">'+item.subject+'</td>' +
                        '                                    <td>'+item.message.slice(0, 10)+'<button type="button" class="show_more" id="contact-show-'+item.id+'">. . .</button></td>' +
                        '                                    <td><i class="fa fa-times c_hidden" id="contact-trash-'+item.id+'" aria-hidden="true" style="font-size:24px"></i></td>' +
                        '                                </tr>');

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    }

    contact_show();

    $("#contact_answer").validate({
        rules:{
            subject:{
                required: true
            },

            message:{
                required: true,
                minlength: 10,
            }
        },
        messages:{
            subject: "<p style='color:red'>please enter Subject<p>",
            message: {
                required: "<p style='color:red'>please enter Message<p>",
                minlength: "<p style='color:red'>please enter minimum 10 words<p>"

            }
        }

    });


    $("#contact_answer").submit(function(event){
        if($("#contact_answer").valid() === true){
            event.preventDefault();
            console.log('a');
            $.ajax({
                url:  $(this).attr('action'),
                type: 'post',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    $('.contact_loading').css('display', 'block');
                },
                success: function () {
                    console.log('success');
                    $('.contact_loading').css('display', 'none');
                    $('#contact_answer_modal').modal('hide');
                    Swal.fire(
                        'Contact answer was send successfully',
                        '',
                        'success'
                    );
                    contact_show();


                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(ajaxOptions);
                    alert(thrownError);
                }
            });
        }
    });

    $(document).on('click', ".show_more", function(){
        console.log('a');
        var id = $(this).attr('id');
        id = id.replace( /^\D+/g, '');
        $('#contact_answer_modal').modal('show');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:"contact/show",
            method:"post",
            dataType: 'JSON',
            data:{id: id},
            success:function(data)
            {
                $('#contact-id').val(id);
                $('.modal-message').html(data.message);
                $('#contact-email').val(data.email);
                $('#contact-name').val(data.name);
            }
        });

    });

    $(document).on('click', ".c_hidden", function(){
        Swal.fire({
            title: 'Are you sure?',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                var id = $(this).attr('id');
                id = id.replace( /^\D+/g, '');
                console.log(id);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: 'contacts/'+id,
                    type: "delete",
                    dataType: 'html',
                    data:{
                        "id": id,
                        "_method": 'DELETE',
                    },
                    success:function(data)
                    {
                        console.log(data);
                        Swal.fire(
                            'Deleted!',
                            'contact has been deleted.',
                            'success'
                        );
                        contact_show();

                    }
                });

            }
        });


    });

});