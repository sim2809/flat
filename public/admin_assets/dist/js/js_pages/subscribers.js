$(document).ready(function(){

    $("#subscribers_email_send").validate({
        rules:{
            subject:{
                required: true
            },

            message:{
                required: true,
                minlength: 10,
            }
        },
        messages:{
            subject: "<p style='color:red'>please enter Subject<p>",
            message: {
                required: "<p style='color:red'>please enter Message<p>",
                minlength: "<p style='color:red'>please enter minimum 10 words<p>"

            }
        }

    });

    $("#subscribers_email_send").submit(function(event){
        event.preventDefault();
        console.log('a');



        if($("#subscribers_email_send").valid() === true){

            $.ajax({
                url:  $(this).attr('action'),
                type: 'post',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function () {
                    console.log("loading");
                    $('#loading').css('display', 'block');
                },
                success: function () {
                    console.log('ok');
                    $('#loading').css('display', 'none');
                    Swal.fire(
                        'Your messages was send successfully',
                        '',
                        'success'
                    );
                }
            });
        }
    });

});