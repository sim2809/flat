
$.validator.addMethod('filesize', function (value, element, arg) {
    var filesize = (element.files.length && element.files[0].size) || false;

    if(!filesize){
        return !filesize
    }

    console.log(filesize);
    var minsize=1000;

    if((filesize>minsize)&&(filesize<=arg)){
        return true;
    }
    else{
        return false;
    }
});



jQuery.validator.addMethod("if_isset", function(value, element, arg) {
    if(arg === 'new'){
        if(($('#new_password').val() === '') && ($('#old_password').val() !== '')){
            return false
        }
        else{
            return true
        }
    }
    if(arg === 'old'){
        if(($('#old_password').val() === '') && ($('#new_password').val() !== '')){
            return false
        }
        else{
            return true
        }
    }
    return false
});