$(document).ready(function(){

    function name_validator(){
        var name = $("input[name=name]");
        var result = $("#name_validate");
        var regex = /^[a-z A-Z]+$/;
        result.text("");
        if(name.val() === ''){
            result.text("name can not be empty");
            result.css("color", "red");
            name.focus();
            return false
        }
        else if (!regex.test(name.val())) {
            result.text("name isn't valid");
            result.css("color", "red");
            name.focus();
            return false
        }
        return true
    }

    function email_validator(){
        var result = $("#email_validate");
        var email = $("input[name=email]");

        var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        result.text("");
        if(email.val() === ''){
            result.text("email can not be empty");
            result.css("color", "red");
            email.focus();
            return false
        }
        else if (!regex.test(email.val())) {
            result.text("email isn't valid");
            result.css("color", "red");
            email.focus();
            return false
        }
        return true
    }

    function subject_validator(){
        var subject = $("input[name=subject]");
        var result = $("#subject_validate");
        result.text("");
        if(subject.val() === ''){
            result.text("subject can not be empty");
            result.css("color", "red");
            subject.focus();
            return false
        }
        else if(subject.val().length > 100){
            result.text("the subject's length should be more than 10 letters");
            result.css("color", "red");
            subject.focus();
            return false
        }
        return true
    }

    function message_validator(){
        var message = $("textarea[name=message]");
        var result = $("#message_validate");
        result.text("");
        if(message.val() === ''){
            result.text("message can not be empty");
            result.css("color", "red");
            message.focus();
            return false
        }
        else if(message.val().length < 10){
            result.text("the message's length should be more than 10 letters");
            result.css("color", "red");
            message.focus();
            return false
        }
        return true
    }


    function contact_validate(){
        if (
            name_validator()&&
            email_validator()&&
            subject_validator()&&
            message_validator()
        )
        {
            return true
        }
        return false
    }

    function sub_validator(){

        var result = $("#sub_validate");
        var email = $("input[name=sub_email]");

        var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        result.text("");
        if(email.val() === ''){
            console.log('bbbbbb');
            result.text("email can not be empty");
            result.css("color", "red");
            email.focus();
            return false
        }
        else if (!regex.test(email.val())) {
            console.log('cccccc');
            result.text("email isn't valid");
            result.css("color", "red");
            email.focus();
            return false
        }
        console.log('aaaaaaaaaa');
        return true
    }

    $("input[name=name]").on("keyup",  name_validator);
    $("input[name=email]").on("keyup",  email_validator);
    $("input[name=subject]").on("keyup",  subject_validator);
    $("textarea[name=message]").on("keyup",  message_validator);



    $("#contact_form").submit(function(event){

        event.preventDefault();

        if(contact_validate()){

            $.ajax({
                url:  $(this).attr('action'),
                type: 'post',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function() {
                    $("#sub_loading").css('display', 'block');
                    $("#sub_no_loading").css('display', 'none');
                },
                success: function(){
                    $("#sub_loading").css('display', 'none');
                    $("#sub_no_loading").css('display', 'block');
                    Swal.fire(
                        'Your message was send successfully',
                        '',
                        'success'
                    );
                    $('#contact_form')[0].reset();
                }});
        }

    });

    $("input[name=sub_email]").on("keyup",  sub_validator);


    $("#subscribe_form").submit(function(event){

        event.preventDefault();

        if(sub_validator()) {

            $.ajax({
                url: $(this).attr('action'),
                type: 'post',
                dataType: 'json',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,


                beforeSend: function () {
                    $('.sub_error').css('display', 'block');
                    $('.sub_error').html('<img src="images/loading.gif" alt="loading..." class="contact_loading" width="38"">');
                },
                success: function (data) {
                    if(data.error){
                        $('.sub_error').css('display', 'block');
                        $('.sub_error').html('<p style="color:red"> this email is already subscribed </p>');
                    }
                    else{
                        $('.sub_error').css('display', 'none');
                        Swal.fire(
                            'Please check your email',
                            '',
                            'success'
                        );
                        $('input[name=sub_email]').val('');
                    }

                }
            });
        }
    });

    $(".portfolio_show").click(function(){
        console.log('hello');
        var id = $(this).attr('id');
        $('#portfolio_modal').modal('show');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "user/portfolio",
            method:"post",
            dataType: 'JSON',
            data:{id: id},
            success:function(data)
            {
                console.log(data.description);
                $('#portfolio-title').html(data.title);
                $('#portfolio-description').html(data.description);
            }
        });

    });

});

