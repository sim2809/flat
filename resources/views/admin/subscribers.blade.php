@extends('admin.template.main')

@section('content')
    <script src="{{asset('admin_assets/dist/js/js_pages/subscribers.js')}}"></script>

    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-5 align-self-center">
                    <h4 class="page-title">Subscribers</h4>
                </div>

            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-12">
                    <div class="card p-4">

                        <div>
                            <button type="button" class="edit float-right" data-toggle="modal" data-target="#exampleModal_3"  ><i class="fa fa-list p_edit" aria-hidden="true" style="font-size:24px"></i></button>
                        </div>

                        <form action="{{route('subscribers.email')}}" method="post" id="subscribers_email_send">
                            @csrf
                            <div class="form-group">
                                <label for="exampleInputTo">To</label>
                                <input  type="text" class="form-control" id="exampleInputTo" value="All Subscribers" readonly="readonly" >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Subject<?php echo $errors->has('subject') ? '<div style="color:red;    display: inline;" >*</div>' : '';?></label>
                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Subject" name="subject" >
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlTextarea">Message<?php echo $errors->has('message') ? '<div style="color:red;    display: inline;" >*</div>' : '';?></label>
                                <textarea class="form-control rounded-0" id="exampleFormControlTextarea" rows="5" placeholder="Enter Message" name="message" ></textarea>
                            </div>

                            <div class="float-right">
                                <button type="submit" class="btn btn-primary float-right">Send Messages</button>
                                <img id="loading" src="{{asset('images/loading.gif')}}" width="40px" height="40px" alt="please wait" style="display: none">
                            </div>

                        </form>
                        @include('admin.layouts.modal_subscribers_list')

{{--                        <div class="table-responsive">--}}
{{--                            <table class="table">--}}
{{--                                <thead>--}}
{{--                                <tr>--}}
{{--                                    <th scope="col">Email</th>--}}
{{--                                    <th></th>--}}
{{--                                </tr>--}}
{{--                                </thead>--}}
{{--                                <tbody>--}}

{{--                                @foreach($subscribers as $subscriber)--}}
{{--                                <tr>--}}

{{--                                    <td>{{$subscriber->email}}</td>--}}
{{--                                    <td   style="float: right; border: 0;">--}}
{{--                                        <i class="fa fa-times p_delete" aria-hidden="true" style="font-size:24px"></i>--}}

{{--                                        <button type="button" class="edit" data-toggle="modal" data-target="#exampleModal_2"  ><i class="fa fa-pencil-square-o p_edit" aria-hidden="true" style="font-size:24px"></i></button>--}}

{{--                                    </td>--}}

{{--                                </tr>--}}
{{--                               @endforeach--}}
{{--                                </tbody>--}}
{{--                            </table>--}}
{{--                        </div>--}}

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
    </div>

    @include('admin.layouts.modal_add_portfolio')
    @include('admin.layouts.modal_edit_portfolio')

@endsection

