@extends('admin.template.main')

@section('content')
    <script src="{{asset('admin_assets/dist/js/js_pages/testimonials.js')}}"></script>

    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-5 align-self-center">
                    <h4 class="page-title">Testimonials</h4>
                </div>

            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-12">
                    <div class="card">

                        <h6 class="card-title m-t-40" style="margin-top: 10px;"><button type="button" class="add" data-toggle="modal" data-target="#add_testimonials_modal" ><i class="fa fa-plus" aria-hidden="true"></i></button></h6>

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">Admin Name</th>
                                    <th scope="col">Text</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody id="testimonial_list">
{{--                                @foreach($portfolios as $portfolio)--}}
{{--                                    <tr>--}}
{{--                                        <td><img src="{{asset('images/portfolio_images/' . $portfolio->image)}}" alt="portfolio image" width="100px"></td>--}}
{{--                                        <td>{{$portfolio->title}}</td>--}}
{{--                                        <td> {{substr($portfolio->description,0,25)}} <button type="button" class="show_more" id="{{$portfolio->id}}">. . .</button></td>--}}
{{--                                        <td>{{$portfolio->category}}</td>--}}
{{--                                        <td   style="float: right; border: 0;">--}}
{{--                                            <button type="button" class="edit" data-toggle="modal" data-target="#exampleModal_2"  ><i class="fa fa-pencil-square-o p_edit" aria-hidden="true" style="font-size:24px"></i></button>--}}
{{--                                            <button type="button" href="{{route('portfolio.destroy', $portfolio->id)}}" class="edit trash_portfolio" id="{{'portfolio-'.$portfolio->id}}"><i class="fa fa-trash p_delete" aria-hidden="true" style="font-size:24px"></i></button>--}}
{{--                                        </td>--}}

{{--                                    </tr>--}}
{{--                                @endforeach--}}
                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
    </div>

    @include('admin.layouts.modal_add_testimonials')
    @include('admin.layouts.modal_testimonial_show')

@endsection