@extends('admin.template.main')

@section('content')
    <script src="{{asset('admin_assets/dist/js/js_pages/profile.js')}}"></script>

    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-5 align-self-center">
                    <h4 class="page-title">Profile</h4>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <!-- Row -->
            <form action="{{route('admin.changes')}}" method="post" class="form-horizontal form-material" id="admin_changes"  enctype="multipart/form-data">
                @csrf
            <div class="row">
                <!-- Column -->
                <div class="col-lg-4 col-xlg-3 col-md-5">
                    <div class="card">
                        <div class="card-body">
                            <center class="m-t-30" id="cropper-div">
                                    <input type="hidden" value="{{asset('admin_assets/assets/images/users/'.$info->image)}}" id="profile_image_old_hidden"/>
                                    <img src="{{asset('admin_assets/assets/images/users/'.$info->image)}}" class="rounded-circle profile_image" width="150" id="profile_image" />

                            </center>
                            <center id="image-div">
                                <h4 class="card-title m-t-10">{{$info->name}}</h4>
                                <label class="btn btn-danger" id="delete_profile_image_btn">
                                    x<input type="button" hidden id="delete_profile_image">
                                </label>
                                <label class="btn btn-danger" id="hide_image_cropper">
                                    x<input type="button" hidden >
                                </label>
                                <label class="btn btn-default" id="add_profile_image_btn">
                                    v<input  type="button" hidden id="add_profile_image">
                                </label>
                            </center>
                        </div>
                        <hr>
                        <div class="card-footer">
                            <center>
                                <label class="btn btn-default" id="image-lable">
                                    Change Profile Image <input  type="file" hidden class='image-validate' id="profile_image_input" name="image">
                                </label>
                            </center>



                        </div>
                    </div>
                </div>
                <!-- Column -->
                <!-- Column -->
                <div class="col-lg-8 col-xlg-9 col-md-7">
                    <div class="card">
                        <div class="card-body" id="valid-1">


                                <div class="form-group">
                                    <label class="col-md-12">Full Name</label>
                                    <div class="col-md-12">
                                        <input type="text" name="name" value="{{$info->name}}" class="form-control form-control-line">
                                        <p id="name_validate" style="color: red"><?php echo $errors->has('name') ? 'please enter your name' : '';?></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Login</label>
                                    <div class="col-md-12">
                                        <input type="text" name="login" value="{{$info->login}}" class="form-control form-control-line">
                                        <p id="login_validate" style="color: red"><?php echo $errors->has('login') ? 'please enter your login' : '';?></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Old Password</label>
                                    <div class="col-md-12">
                                        <input type="password" name="old_password" class="form-control form-control-line" id="old_password">
                                        <p id="first_p_validate" style="color: red"><?php echo $errors->has('old_password') ? 'please enter your old password' : '';?></p>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">New Password</label>
                                    <div class="col-md-12">
                                        <input type="password" name="new_password" class="form-control form-control-line" id="new_password">
                                        <p id="second_p_validate" style="color:red"><?php echo (session()->has('password_error')) ? session()->get('password_error') : '';?></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="profile_image" id="hidden_image_input">
                                </div>

                                <div class="form-group float-right">
                                    <div class="col-sm-12">
                                        <img id="changes_loading" src="{{asset('images/loading.gif')}}" alt="loading..." class="contact_loading" width="25" style="display: none;">
                                        <button type="submit" class="btn btn-primary">Save Changes</button>
                                    </div>
                                    @if($message = Session::get('login_error'))
                                        <p style="color:red">{{$message}}</p>
                                    @endif
                                </div>

                        </div>
                    </div>
                </div>
                <!-- Column -->
            </div>
            </form>
            <!-- Row -->
            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Right sidebar -->
            <!-- ============================================================== -->
            <!-- .right-sidebar -->
            <!-- ============================================================== -->
            <!-- End Right sidebar -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>

    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
    </div>

    @include('admin.layouts.modal_add_portfolio')
    @include('admin.layouts.modal_edit_portfolio')

@endsection
