            <div class="modal fade" id="add_portfolio_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Portfolio</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

    <div class="modal-body">
        <form action="{{route('portfolio.store')}}" id="add_portfolio_form">
          @csrf
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Title:</label>
            <input type="text" class="form-control" id="recipient-name" name="title" >
            <p style="color: red" id="portfolio_error"></p>
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Description:</label>
            <textarea class="form-control" id="message-text" name="description"></textarea>
          </div>
          <div class="form-group">
            <select name="category_id" class="form-control" id="category_p_select">
              <option value="1">uncategory</option>
            </select>

          </div>
          <div class="form-group">
          <div class="custom-file mb-3" style="margin-top: 14px">
            <input type="file" class="custom-file-input" name="file" id="portfolio-add-img">
            <label class="custom-file-label" for="customFile" >Choose file</label>
        {{--      <input type="file" name="file" id="profile-img">--}}
            <img src="" id="portfolio-add-img-tag" width="100px" alt="hear will be image" />

          </div>
          </div>




          <button type="submit" class="btn btn-success float-right">Add Portfolio</button>

        </form>


      </div>
    </div>
  </div>
</div>
