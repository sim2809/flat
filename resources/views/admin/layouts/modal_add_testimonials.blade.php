<div class="modal fade" id="add_testimonials_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Testimonial</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

    <div class="modal-body">
        <form action="{{route('testimonials.store')}}" id="add_testimonial_form">
          @csrf
          <div class="form-group">
            <label for="testimonial-text" class="col-form-label">Testimonial:</label>
            <textarea class="form-control" id="testimonial-text" name="testimonial"></textarea>
          </div>
          <button type="submit" class="btn btn-success float-right">Add Testimonial</button>
        </form>


      </div>
    </div>
  </div>
</div>