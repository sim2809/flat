<!-- Modal -->
<div class="modal fade" id="contact_answer_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body modal-content">
          <p class="modal-message">
        </p>


        <form action="{{route('contact.answer')}}" method="post" id="contact_answer">
          @csrf
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Subject:</label>
            <input type="text" class="form-control" id="recipient-name" value="Contact Answer"  name="subject">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Message:</label>
            <textarea class="form-control" id="message-text" name="message" ></textarea>
          </div>

          <input type="hidden" id="contact-id" name="id">
          <input type="hidden" id="contact-name" name="name">
          <input type="hidden" id="contact-email" name="email">
          <div class="modal-footer">
            <img src="{{asset('images/loading.gif')}}" alt="loading..." class="contact_loading" width="38" style="display: none;">
            <button type="submit" class="btn btn-primary">Send message</button>
          </div>
        </form>


    </div>
  </div>
</div>
</div>