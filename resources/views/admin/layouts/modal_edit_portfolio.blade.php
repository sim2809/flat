<div class="modal fade" id="edit_portfolio_modal" tabindex="-1" role="dialog" aria-labelledby="edit_portfolio_modal_Lable" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Portfolio</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>


        <div class="modal-body">
            <form action="{{route('portfolio.update')}}" id="edit_portfolio_form">
                @csrf
                <input type="hidden" id="edit_portfolio_id" name="portfolio_id" >
                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Title:</label>
                    <input type="text" class="form-control" id="edit_portfolio_title" name="title" >
                    <p id="title_error" style="color:red"></p>
                </div>
                <div class="form-group">
                    <label for="message-text" class="col-form-label">Description:</label>
                    <textarea class="form-control" id="edit_portfolio_description" name="description"></textarea>
                    <p id="description_error" style="color:red"></p>
                </div>
                <div class="form-group">
                    <select name="category_id" class="form-control" id="category_p_select_edit">
                        <option value="1">uncategory</option>
                    </select>

                </div>
                <div class="form-group">
                    <div class="custom-file mb-3" style="margin-top: 14px">
                        <input type="file" class="custom-file-input" name="file" id="portfolio-edit-img">
                        <label class="custom-file-label" for="customFile" >Change image</label>
                        {{--      <input type="file" name="file" id="profile-img">--}}
                        <img src="" id="portfolio-img-tag" width="100px" alt="hear will be image" />

                    </div>
                </div>

                <button type="submit" class="btn btn-success float-right">Save Edits</button>
            </form>


        </div>

    </div>
  </div>
</div>
