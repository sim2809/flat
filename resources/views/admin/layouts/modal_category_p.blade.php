<!-- Modal -->
<div class="modal fade" id="category_p_modal" tabindex="-1" role="dialog" aria-labelledby="category_p" aria-hidden="true" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Portfolio Categories</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">

        <div id="category_p_list">
        </div>
        <hr>
        <form action="{{route('category.store')}}" id="add_category_form">
          @csrf
          <input type="text" name="category_name" placeholder="Category name" id="subemail">
          <button type="submit" class="btn btn-primary">+</button>
        </form>
      <div class="category_error"></div>





    </div>
  </div>
</div>
</div>

