@extends('admin.template.main')

@section('content')
    <script src="{{asset('admin_assets/dist/js/js_pages/portfolio.js')}}"></script>

    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-5 align-self-center">
                    <h4 class="page-title">Portfolio</h4>
                </div>

            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div style="display: inline">
                            <div class="float-left"><button type="button" class="add" data-toggle="modal" data-target="#add_portfolio_modal" ><i class="fa fa-plus" aria-hidden="true"></i></button></div>
                            <div class="float-right"><button type="button" class="add" data-toggle="modal" data-target="#category_p_modal" ><i class="mdi mdi-layers"></i></button></div>
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Category</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody id="portfolio_list">
{{--                                @foreach($portfolios as $portfolio)--}}
{{--                                    <tr>--}}
{{--                                        <td><img src="{{asset('images/portfolio_images/' . $portfolio->image)}}" alt="portfolio image" width="100px"></td>--}}
{{--                                        <td>{{$portfolio->title}}</td>--}}
{{--                                        <td> {{substr($portfolio->description,0,25)}} <button type="button" class="show_more" id="{{$portfolio->id}}">. . .</button></td>--}}
{{--                                        <td>{{$portfolio->category}}</td>--}}
{{--                                        <td   style="float: right; border: 0;">--}}
{{--                                            <button type="button" class="edit" data-toggle="modal" data-target="#exampleModal_2"  ><i class="fa fa-pencil-square-o p_edit" aria-hidden="true" style="font-size:24px"></i></button>--}}
{{--                                            <button type="button" href="{{route('portfolio.destroy', $portfolio->id)}}" class="edit trash_portfolio" id="{{'portfolio-'.$portfolio->id}}"><i class="fa fa-trash p_delete" aria-hidden="true" style="font-size:24px"></i></button>--}}
{{--                                        </td>--}}

{{--                                    </tr>--}}<i class="fas fa-network-wired"></i>
{{--                                @endforeach--}}
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
    </div>
    @include('admin.layouts.modal_category_p')
    @include('admin.layouts.modal_add_portfolio')
    @include('admin.layouts.modal_edit_portfolio')

@endsection