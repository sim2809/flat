<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
@include('layouts.head')

    <script>





    </script>

</head>
<body onload="setTimeout(function() { window.scrollTo(0, 1) }, 100);">
<!----start-container----->
<div class="head-bg sTop">
    <div class="conatiner">
        <div class="col-lg-12 header-note">
            <a href="#"><img src="images/logo.png" title="Flat style" /></a>
            <h1>We are a Creative Digital Agency</h1>
        </div>
    </div>
</div>
<!----//End-container----->
<!----start-top-nav---->
<nav class="subMenu navbar-custom navbar-scroll-top" role="navigation">
    <div class="container">
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                <img src="images/nav-icon.png" title="drop-down-menu" />
            </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-left navbar-main-collapse">
            <ul class="nav navbar-nav">
                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                <li class="active">
                    <a id="sTop" class="subNavBtn" href="#">Home</a>
                </li>
                <li class="page-scroll">
                    <a id="s1" class="subNavBtn" href="#">About</a>
                </li>
                <li class="page-scroll">
                    <a id="s2" class="subNavBtn" href="#">Portfolio</a>
                </li>
                <li class="page-scroll">
                    <a id="s3" class="subNavBtn" href="#">Testimonials</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
        <a  id="s4" class="right-msg subNavBtn msg-icon"href="#"><span> </span></a>
        <div class="clearfix"> </div>
    </div>
    <!-- /.container -->
</nav>
<!----//End-top-nav---->
<!---- start-top-grids---->
<div class="container">
    <div class="row  section s1 top-grids">
        <div class="col-md-3 top-grid">
            <span class="icon1"> </span>
            <h2>Responsive Design</h2>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
        </div>
        <div class="col-md-3 top-grid">
            <span class="icon2"> </span>
            <h2>Web Development</h2>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
        </div>
        <div class="col-md-3 top-grid">
            <span class="icon3"> </span>
            <h2>Internet Marketing</h2>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
        </div>
        <div class="col-md-3 top-grid">
            <span class="icon4"> </span>
            <h2>IOS Development</h2>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
<!---- //End-top-grids---->
<!----start-about---->
<div class="about">
    <div class="container">
        <div class="col-md-6 divice">
            <img class="img-responsive" src="images/divice.png" title="divice" />
        </div>
        <div class="col-md-6 divice-info">
            <h3>Responsive Design</h3>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
            <a class="btn btn-primary btn-red" href="#">Read More <span> </span></a>
        </div>
    </div>
</div>
<!----//End-about---->
<!----start-portfolio---->
<div class="portfolio section s2">
    <div class="container portfolio-head">
        <h3> Portfolio</h3>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
    </div>
    <!---- start-portfolio-script----->
    <script src="js/hover_pack.js"></script>
    <script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
    <script type="text/javascript">
        $(function () {
            var filterList = {
                init: function () {

                    // MixItUp plugin
                    // http://mixitup.io
                    $('#portfoliolist').mixitup({
                        targetSelector: '.portfolio',
                        filterSelector: '.filter',
                        effects: ['fade'],
                        easing: 'snap',
                        // call the hover effect
                        onMixEnd: filterList.hoverEffect()
                    });

                },
                hoverEffect: function () {
                    // Simple parallax effect
                    $('#portfoliolist .portfolio').hover(
                        function () {
                            $(this).find('.label').stop().animate({bottom: 0}, 200, 'easeOutQuad');
                            $(this).find('img').stop().animate({top: -30}, 500, 'easeOutQuad');
                        },
                        function () {
                            $(this).find('.label').stop().animate({bottom: -40}, 200, 'easeInQuad');
                            $(this).find('img').stop().animate({top: 0}, 300, 'easeOutQuad');
                        }
                    );

                }

            };
            // Run the show!
            filterList.init();
        });
    </script>
    <!----//End-portfolio-script----->
    <ul id="filters" class="clearfix">
        <li><span class="filter active" data-filter="{{$all_categories_ids}}">All</span></li>
        @foreach($categories as $index => $category)
            @if($index != 0)
            <li><span class="filter" data-filter="{{$category->id}}">{{$category->name}}</span></li>
            @endif
        @endforeach
    </ul>
    <div id="portfoliolist">
        @foreach($portfolios as $portfolio)
            <div class="portfolio {{$portfolio->category_id}} mix_all" style="display: inline-block; opacity: 1;">
                <div class="portfolio-wrapper">
                    <a data-toggle="modal"  href="#" class="b-link-stripe b-animate-go  thickbox portfolio_show" id="{{$portfolio->id}}">
                        <img src="{{asset('images/portfolio_images/' . $portfolio->image)}}" width="360" height="270"/><div class="b-wrapper"><h2 class="b-animate b-from-left    b-delay03 "><img src="images/link-ico.png" alt=""/></h2>
                        </div></a>
                </div>
            </div>
        @endforeach
        <div class="clearfix"> </div>
    </div>
</div>
<!----//End-portfolio---->
<!---testmonials---->
<div  class="testmonials section s3">
    <div class="container">
        <div class="bs-example">
            <div id="myCarousel" class="carousel slide" data-interval="3000" data-ride="carousel">
                <!-- Carousel indicators -->
                <ol class="carousel-indicators pagenate-icons">
                    @foreach($testimonials as $key=>$testimonial)
                        <li data-target="#myCarousel" data-slide-to="{{$key}}" class="{{ $key == 0 ? "active" : "" }}"></li>
                    @endforeach
                </ol>
                <!-- Carousel items -->
                <div class="carousel-inner">
                    @foreach($testimonials as $key=>$testimonial)
                        <div class="{{ $key == 0 ? "active" : "" }} item">
                            <h2><img src="{{asset('admin_assets/assets/images/users/'.$testimonial->image)}}" title="name" width="48px"/></h2>
                            <div class="carousel-caption caption">
                                <h3>{{$testimonial->name}}</h3>
                                <p>{{$testimonial->text}}</p>
                            </div>
                        </div>
                    @endforeach

                </div>
                <!-- Carousel nav -->
            </div>
        </div>
    </div>
</div>
<!---testmonials---->
<!----start-model-box---->
<a data-toggle="modal" data-target=".bs-example-modal-md" href="#"> </a>
<div class="modal fade bs-example-modal-md light-box" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="portfolio_modal" style="word-wrap: break-word">
    <div class="modal-dialog modal-md">
        <div class="modal-content light-box-info">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="images/close.png" title="close" /></button>
            <h3 id="portfolio-title"></h3>
            <p  id="portfolio-description"></p>
        </div>
    </div>
</div>
<!----start-model-box---->
<!----start-contact---->
@include('layouts.footer')
<!----//End-contact---->
<!-- Include all compiled plugins (below), or include individual files as needed -->

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript" 	src="js/jquery.ajax.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
    @if(session()->has('error'))
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'token was wrong!',
        });
    @elseif(session()->has('success'))
        Swal.fire({
            type: 'success',
            title: 'Subscribing completed successfully',
            text: '',
        });
    @endif
</script>
</body>
</html>