
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <title>Document</title>
</head>
<body>

        <div class="container py-5">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6 mx-auto">


                            <div class="card rounded-0">
                                <div class="card-header">
                                    <h3 class="mb-0">Login</h3>


                                    @if($errors->any())
                                        @foreach($errors->all() as $error)
                                            <p style="color:red">{{$error}}</p>
                                        @endforeach
                                    @endif

                                </div>
                                <div class="card-body">

                                    <!-- form-start-->
                                    <form class="form" role="form" action="{{route('login.check')}}"  method="POST">
                                        @csrf
                                        <div class="form-group">

                                            <label for="login">Login</label>

                                            <input type="text" class="form-control form-control-lg rounded-0" name="login" id="login">

                                            <div class="invalid-feedback">Oops, you missed this one.</div>
                                        </div>


                                        <div class="form-group">
                                            <label>Password</label>

                                            <input type="password" class="form-control form-control-lg rounded-0" id="pwd1"  autocomplete="new-password" name="password">

                                            <div class="invalid-feedback">Enter your password too!</div>
                                        </div>

                                        <button type="submit" class="btn btn-success btn-lg float-right" id="btnLogin">Login</button>
                                    </form>

                                    <!-- form-end-->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</body>
</html>