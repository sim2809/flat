<div class="contact section s4">
    <div class="container">
        <h4>Contact</h4>
        <p class="contact-head">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
        <div class="row contact-form">
            <form id="contact_form" action="{{route('contact.send')}}">
                @csrf
                <div class="col-md-6 text-box">
                    <input type="text" <?php echo $errors->has('name') ? 'style="border-color: red;"' : '';?> placeholder="Name" name="name"  value="{{old('name')}}" />
                    <p id='name_validate'></p>
                    <input type="text" <?php echo $errors->has('email') ? 'style="border-color: red;"' : '';?> placeholder="Email" name="email" value="{{old('email')}}" />
                    <p id='email_validate'></p>
                    <input type="text" <?php echo $errors->has('subject') ? 'style="border-color: red;"' : '';?> placeholder="Subject" name="subject" value="{{old('subject')}}" />
                    <p id='subject_validate'></p>
                </div>
                <div class="col-md-6 textarea">
                    <textarea <?php echo $errors->has('message') ? 'style="border-color: red;"' : '';?> name="message" placeholder="Message" >{{old('message')}}</textarea>
                    <p id='message_validate'></p>
                </div>
                <div class="clearfix"><p id="s_e" style="font-size: 20px;"><img src="{{asset('images/loading.gif')}}" width="40px" height="40px" alt="please wait" style="display: none"></p> </div><br />

                <button style="margin-top: 10px" id="sub_no_loading" class="btn btn-primary btn-red-lg" type="submit">Send Contact</button>
                <button id="sub_loading" style="display: none" class="btn btn-primary btn-red-lg" type="button"><img src="{{asset('images/loading.gif')}}" width="25px" height="25px" alt="please wait"></button>
            </form>
        </div>
        <!----start-social-icons--->
        <div class="social-icons" style="margin-bottom: 10px;">
            <ul>
                <li><a class="facebook" href="#"> <span> </span></a></li>
                <li><a class="twitter" href="#"> <span> </span></a></li>
                <li><a class="dribbble" href="#"> <span> </span></a></li>
            </ul>
        </div>
        <!----//End-social-icons--->
        <!----start-copy-right---->
        <div class="copy-right">

            <form id="subscribe_form" style="margin-bottom: 5px" action="{{route('subscribe.store')}}">
                @csrf
                <input type="text" name="sub_email" placeholder="Enter Your Email" id="subemail">
                <button type="submit" class="btn btn-dark sub" id="subscribe">Subscribe</button>
                <div class="sub_error"><p id="sub_validate"></p></div>
            </form>


            <p>Copyright &#169; 2019</p>

        </div>

    </div>
</div>